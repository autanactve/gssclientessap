/*
 * Generated by XDoclet - Do not edit!
 */
package net.ws.client.interfaces;

/**
 * Local interface for zws_ps_gis.
 * @xdoclet-generated at ${TODAY}
 * @copyright The XDoclet Team
 * @author XDoclet
 * @version ${version}
 */
public interface zws_ps_gisLocal
   extends javax.ejb.EJBLocalObject, com.gesmallworld.gss.lib.service.ServiceLocal
{
   /**
    * Generated service proxy method. Corresponds to a service of a Magik Service Provider.
    * @param request A request object as specified in the service description file.
    * @return a business response    */
   public com.gesmallworld.gss.lib.request.Response ZpsRfcCreaManualUucc( com.gesmallworld.gss.lib.request.Request request ) ;

   /**
    * Generated service proxy method. Corresponds to a service of a Magik Service Provider.
    * @param request A request object as specified in the service description file.
    * @return a business response    */
   public com.gesmallworld.gss.lib.request.Response ZpsRfcCostosPlan( com.gesmallworld.gss.lib.request.Request request ) ;

   /**
    * Generated service proxy method. Corresponds to a service of a Magik Service Provider.
    * @param request A request object as specified in the service description file.
    * @return a business response    */
   public com.gesmallworld.gss.lib.request.Response ZpsRfcValorizacionUucc( com.gesmallworld.gss.lib.request.Request request ) ;

   /**
    * Generated service proxy method. Corresponds to a service of a Magik Service Provider.
    * @param request A request object as specified in the service description file.
    * @return a business response    */
   public com.gesmallworld.gss.lib.request.Response ZpsRfcValidarPep( com.gesmallworld.gss.lib.request.Request request ) ;

}
