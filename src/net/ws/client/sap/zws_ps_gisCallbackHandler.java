
/**
 * Zws_ps_gisCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package net.ws.client.sap;

import net.ws.client.sap.zws_ps_gisStub.ZpsRfcValorizacionUuccResponse;

    /**
     *  Zws_ps_gisCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class zws_ps_gisCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public zws_ps_gisCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public zws_ps_gisCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for ZpsRfcCreaManualUucc method
            * override this method for handling normal response from ZpsRfcCreaManualUucc operation
            */
           public void receiveResultZpsRfcCreaManualUucc(
        		   net.ws.client.sap.zws_ps_gisStub.ZpsRfcCreaManualUuccResponse result
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from ZpsRfcCreaManualUucc operation
           */
            public void receiveErrorZpsRfcCreaManualUucc(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for ZpsRfcCostosPlan method
            * override this method for handling normal response from ZpsRfcCostosPlan operation
            */
           public void receiveResultZpsRfcCostosPlan(
                    net.ws.client.sap.zws_ps_gisStub.ZpsTtCostosPlan result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from ZpsRfcCostosPlan operation
           */
            public void receiveErrorZpsRfcCostosPlan(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for ZpsRfcValorizacionUucc method
            * override this method for handling normal response from ZpsRfcValorizacionUucc operation
            */
           public void receiveResultZpsRfcValorizacionUucc(
        		   net.ws.client.sap.zws_ps_gisStub.ZpsRfcValorizacionUuccResponse result
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from ZpsRfcValorizacionUucc operation
           */
            public void receiveErrorZpsRfcValorizacionUucc(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for ZpsRfcValidarPep method
            * override this method for handling normal response from ZpsRfcValidarPep operation
            */
           public void receiveResultZpsRfcValidarPep(
                    net.ws.client.sap.zws_ps_gisStub.String result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from ZpsRfcValidarPep operation
           */
            public void receiveErrorZpsRfcValidarPep(java.lang.Exception e) {
            }
                


    }
    