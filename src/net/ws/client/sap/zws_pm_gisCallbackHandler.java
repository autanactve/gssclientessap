
/**
 * Zws_pm_gisCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package net.ws.client.sap;

    /**
     *  Zws_pm_gisCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class zws_pm_gisCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public zws_pm_gisCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public zws_pm_gisCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for ZpmRfcCuadraturaUucc method
            * override this method for handling normal response from ZpmRfcCuadraturaUucc operation
            */
           public void receiveResultZpmRfcCuadraturaUucc(
                    net.ws.client.sap.zws_pm_gisStub.ZpmTtCuadraturaUucc result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from ZpmRfcCuadraturaUucc operation
           */
            public void receiveErrorZpmRfcCuadraturaUucc(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for ZpmRfcValidarOt method
            * override this method for handling normal response from ZpmRfcValidarOt operation
            */
           public void receiveResultZpmRfcValidarOt(
                    net.ws.client.sap.zws_pm_gisStub.String result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from ZpmRfcValidarOt operation
           */
            public void receiveErrorZpmRfcValidarOt(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for ZpmRfcCreaOtUucc method
            * override this method for handling normal response from ZpmRfcCreaOtUucc operation
            */
           public void receiveResultZpmRfcCreaOtUucc(
                    net.ws.client.sap.zws_pm_gisStub.ZpmTtCreaOtUuccResp result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from ZpmRfcCreaOtUucc operation
           */
            public void receiveErrorZpmRfcCreaOtUucc(java.lang.Exception e) {
            }
                


    }
    