/**
 * Clase que carga los par�metros de configuraci�n
 * @author Freddy Banquez
 * @creado 22-05-2017
 * @ultimamodificacion 22-05-2017
 * @historial
 * 			22-05-2017 FMB Primera versi�n.
 **/

package net.ws.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;


public final class StoreConfigParameters {
	private static volatile StoreConfigParameters instance = null;
	private Properties properties = null;
	
	
	/**
	 * Constructor de la clase con el modificador de acceso en privado para
	 * prevenir que cualquier otra clase pueda instanciarla (Singleton).
	 * 
	 **/
	private StoreConfigParameters() {
		
		InputStream input = null;
		File file = null;
		String pathName = null;
		this.properties = new Properties();

		// Ruta del archivo de Configuraciones
		String configFile = System.getenv("JBOSS_HOME") + File.separator + "server" + File.separator + 
				"default" + File.separator + "deploy" + File.separator + "InitialsParameters.config";
		
		try {
			
			file = new File( configFile );
			pathName = file.getCanonicalPath();
			input = new FileInputStream( configFile );
			properties.load( input );
			
		} catch( FileNotFoundException e ) {
			System.out.println( MessageFormat.format( "El archivo InitialsParameters.config no fue conseguido en la ruta {0} o no se puede leer.", pathName ) );
			System.out.println( "StoreConfigParameters:StoreConfigParameters:FileNotFoundException: " + e.getMessage() );
		} catch( IOException e ) {
			System.out.println( MessageFormat.format( "Ocurrio un error al leer el archivo {0}.", file.getName() ) );
			System.out.println( "StoreConfigParameters:StoreConfigParameters:IOException: " + e.getMessage() );
		} catch( NullPointerException e ) { 
			System.out.println( MessageFormat.format( "No es posible hallar la ruta del archivo {0}.", file.getName() ) );
			System.out.println( "StoreConfigParameters:StoreConfigParameters:NullPointerException: " + e.getMessage() );
		} finally {
			
			if ( input != null ) {
				try {
					input.close();
				} catch ( IOException e ) {
					System.out.println( MessageFormat.format( "Error al cerrar el archivo {0}.", file.getName() ) );
					System.out.println( "StoreConfigParameters:StoreConfigParameters:finally:IOException: " + e.getMessage() );
				}
			}
			
		}//Cierre del finally
		
	}
	
	
	/**
	 * Funci�n que retorna una �nica instancia de StoreConfigParameters.
	 * @return instance
	 * 			Instancia del tipo StoreConfigParameters().
	 * 
	 **/
	public static synchronized StoreConfigParameters getInstance() {
		
		if ( instance == null ) {
			synchronized (StoreConfigParameters.class) {
                if( instance == null ) 
                    instance = new StoreConfigParameters();
            }
        }
		
        return instance;
	}
	
	
	/**
	 * Funci�n que sobreescribe el m�todo clone() para evitar tener m�s de una
	 * instancia de la clase StoreConfigParameters.
	 * @throws 
	 * 		CloneNotSupportedException
	 * 
	 **/
	@Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }
	
	
	/**
	 * Funci�n que permite obtener el valor de una configuraci�n dada su clave.
	 * @param key
	 * 			Clave del atributo a buscar
	 * @return value
	 * 			Valor del atributo buscado
	 * 
	 **/
	public String getAttributeFromProperties(String key) {
		
		String value = this.properties.getProperty( key );
		
		if( value == null || value.isEmpty() )
			System.out.println( MessageFormat.format( "El valor de {0} es nulo o vacio.", key ) );
		
		return value;
	}

}
